/*
 * 
 * All the resources for this project: https://randomnerdtutorials.com/
 * Modified by Rui Santos
 * 
 * Created by FILIPEFLOP
 * 
 */
 
#include <SPI.h>
#include <MFRC522.h>
 
#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
int led = 6;
const int buzzer = 8;
int attempt = 0;

void setup() 
{
  Serial.begin(9600);   // Initiate a serial communication
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  pinMode(led, OUTPUT);  
  Serial.println("Approximate your card to the reader...");
  Serial.println();
  pinMode(buzzer, OUTPUT); // Set buzzer - pin 9 as an output
}
void loop() 
{
  char inByte;
  
  /*
  if(Serial.available()){
    inByte = Serial.read();
  }

  if(inByte == '1') {
    Serial.println("Sukses");
  }
  */
  
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  
  //Show UID on serial monitor
  //Serial.print("UID tag :");
  String content= "";
  String rfid_before = "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  content.toUpperCase();

  if  (content == rfid_before) {
    attempt++;
  }
  else {
    attempt = 0;
  }
  rfid_before = content;
  
  delay(2500);

  inByte = Serial.read();

  if (inByte == '1') {
    Serial.println("Authorized access");
    digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);               // wait for a second
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    delay(1000);               // wait for a second
  }

  else {
    Serial.println(" Access denied");
    tone(buzzer, 1000); // Send 1KHz sound signal...
    delay(1000);        // ...for 1 sec
    noTone(buzzer);     // Stop sound...
    delay(1000);        // ...for 1sec
    if (attempt > 2) {
        //Kirim notifikasi ke satpam
        Serial.println(" Sent Alert");
      } 
  }

  /*
    if (content.substring(1) == "5D F8 BA 11") //change here the UID of the card/cards that you want to give access
    {
      Serial.println("Authorized access");
      Serial.println();
      digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);               // wait for a second
      digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
      delay(3000);
    }
 
    else {
      Serial.println(" Access denied");
      delay(3000);
      if (error > 1) {
        Serial.println("Sent Alert");
        tone(buzzer, 1000); // Send 1KHz sound signal...
        delay(5000);        // ...for 1 sec
        noTone(buzzer);     // Stop sound...
        delay(500);        // ...for 1sec
      } 
      error ++;
    }
    */
} 
