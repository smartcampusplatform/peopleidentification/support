import serial
import requests
import datetime
import time
#from requests.auth import HTTPProxyAuth

portNumber="COM5"
baudrate=9600

locationID="lctn1"
service_server='https://servicereksti.herokuapp.com/'
db_server='https://dbreksti.herokuapp.com/'

#http_proxy="http://cache.itb.ac.id:8080"
#https_proxy="http://cache.itb.ac.id:8080"
#proxy_dict={"http":http_proxy,"https":https_proxy}
#auth=HTTPProxyAuth("varrelkusuma","58070811")

arduino = serial.Serial(portNumber,baudrate,timeout=.1)

previous=None
attempt=0

while True:
    data=arduino.readline()
    if data and len(data[1:-2])==11:
        raw=str(data[1:-2])
        cardID="".join([raw[2],raw[3],raw[5],raw[6],raw[8],raw[9],raw[11],raw[12]])
        
        if(cardID==previous):
            attempt+=1
        else:
            attempt=0

        if(attempt>1):
            alert="True"
        else:
            alert="False"
        print("alert : ", alert)

        ts = time.time()
        timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        url=service_server+'sendTapData?cardID={}&locationID={}&timestamp={}&alert={}'.format(cardID,locationID,timestamp,alert)
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}
        r = requests.get(url,headers=headers)#,proxies=proxy_dict,auth=auth)
        print(r.text)
        if(r.text=="True"):
            arduino.write(b'1')        
        else:
            arduino.write(b'0')
        previous=cardID




        
        